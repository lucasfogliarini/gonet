﻿using Autofac;
using Autofac.Integration.WebApi;
using Owin;
using System;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin;

namespace GoNET.Api.Owin
{
    using OAuth;
    public abstract class OwinStart : AutofacStart
    {
        private IAppBuilder OwinApp { get; set; }
        public void Configuration(IAppBuilder owinApp)
        {
            OwinApp = owinApp;
            Up();//first
            ConfigureOAuth(TimeSpan.FromMinutes(30));//mid
            //OwinApp.UseCors(CorsOptions.AllowAll);//mid
            OwinApp.UseWebApi(HttpConfiguration);//last
        }

        private void ConfigureOAuth(TimeSpan accessTokenExpireTimeSpan)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = accessTokenExpireTimeSpan,
                Provider = HttpConfiguration.DependencyResolver.GetService(typeof(OAuthProvider)) as OAuthProvider,
                RefreshTokenProvider = HttpConfiguration.DependencyResolver.GetService(typeof(OAuthRefreshTokenProvider)) as OAuthRefreshTokenProvider
            };

            // Token Generation
            OwinApp.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            OwinApp.UseOAuthAuthorizationServer(OAuthServerOptions);
        }
        
        protected override void RegisterDependencies(ContainerBuilder containerBuilder)
        {
            base.RegisterDependencies(containerBuilder);
            containerBuilder.RegisterType<OAuthProvider>();
            containerBuilder.RegisterType<OAuthRefreshTokenProvider>();
            IContainer container = containerBuilder.Build();
            HttpConfiguration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            OwinApp.UseAutofacMiddleware(container);
            OwinApp.UseAutofacWebApi(HttpConfiguration);
        }
    }
}