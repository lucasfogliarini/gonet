﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoNET.Api.Owin.OAuth
{
    using Exceptions;
    using Microsoft.Owin;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.OAuth;
    using GoNET.Authentication;
    using Services.Authentication;
    public class OAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly IAuthenticationService _authenticationService;
        private UserIdentity _userIdentity;
        private IAppClient _appClient;

        public OAuthProvider(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            try
            {
                _appClient = _authenticationService.GetAppClient(clientId);
                context.OwinContext.Set("as:clientAllowedOrigin", _appClient.AllowedOrigin);
                context.OwinContext.Set("as:clientRefreshTokenLifeTime", _appClient.RefreshTokenLifeTime.ToString());
                context.Validated();
            }
            catch (BaseException ex)
            {
                context.Response.StatusCode = (int)ex.MessageCode.StatusCode();
                context.SetError(ex.Code, ex.Message);
            }

            return Task.FromResult<object>(null);
        }        
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
                context.OwinContext.Response.Headers.Append("Access-Control-Allow-Origin", allowedOrigin ?? "*");
                _userIdentity = _authenticationService.Authorize(context.UserName);

                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                        "as:client_id", context.ClientId ?? string.Empty
                    }
                });

                var ticket = new AuthenticationTicket(_userIdentity, props);
                context.Validated(ticket);
            }
            catch (BaseException ex)
            {
                context.Response.StatusCode = (int)ex.MessageCode.StatusCode();
                context.SetError(ex.Code, ex.Message);
            }

            return Task.FromResult<object>(null);
        }
        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            var currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                var message = new MessageCode(m=>m.E013);
                context.Response.StatusCode = (int)message.StatusCode();
                context.SetError(message.Code, message.Message);
                return Task.FromResult<object>(null);
            }
            var newIdentity = _authenticationService.Authorize(context.Ticket.Identity.Name);
            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }
        private FormCollection GetOwinFormCollection(IOwinRequest request)
        {
            return request.Environment["Microsoft.Owin.Form#collection"] as FormCollection;
        }
    }
}