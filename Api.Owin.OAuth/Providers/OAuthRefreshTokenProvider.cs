﻿using GoNET.Authentication;
using GoNET.Services.Authentication;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Threading.Tasks;

namespace GoNET.Api.Owin.OAuth
{
    public class OAuthRefreshTokenProvider : IAuthenticationTokenProvider
    {
        private readonly IAuthenticationService _authenticationService;
        public OAuthRefreshTokenProvider(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var clientid = context.Ticket.Properties.Dictionary["as:client_id"];

            if (string.IsNullOrEmpty(clientid))
            {
                return Task.FromResult<object>(null);
            }

            string refreshTokenLifeTime = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime");
            DateTime issuedDate = DateTime.UtcNow;
            DateTime expiresDate = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime));
            context.Ticket.Properties.IssuedUtc = issuedDate;
            context.Ticket.Properties.ExpiresUtc = expiresDate;

            IRefreshToken token = _authenticationService.CreateRefreshToken(clientid, context.Ticket.Identity.Name, issuedDate, expiresDate, context.SerializeTicket());
            
            context.SetToken(token.RefreshTokenId);
            return Task.FromResult<object>(null);
        }

        public Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            context.OwinContext.Response.Headers.Append("Access-Control-Allow-Origin", allowedOrigin);

            string protectedTicket = _authenticationService.RemoveRefreshToken(context.Token);

            if (protectedTicket != null)
            {
                context.DeserializeTicket(protectedTicket);
            }
            return Task.FromResult<object>(null);
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }
    }
}