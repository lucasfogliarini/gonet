﻿using System;

namespace GoNET.Data.Seeds
{
    public class SeedHistory
    {
        public SeedHistory(Seed seed)
        {
            SeedId = seed.GetType().Name;
            ContextKey = seed.GetType().Namespace;
            RunDate = DateTime.Now;
        }
        public string SeedId { get; private set; }
        public string ContextKey { get; private set; }
        public DateTime RunDate { get; private set; }
    }
}
