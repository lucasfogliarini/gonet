﻿using System.Data.Entity;

namespace GoNET.Data.EntityFramework
{

    public abstract class Repository<TDatabase,TEntity> : ReadOnlyRepository<TDatabase,TEntity> 
        where TEntity : Entity
        where TDatabase : Database
    {
        protected Repository(IDatabaseFactory<TDatabase> databaseFactory) : base(databaseFactory)
        {
        }        
        public TEntity Add(TEntity entity)
        {
            return this.Set.Add(entity);
        }        
        public void Update(TEntity entity)
        {
            this.Set.Attach(entity);
            this.DbContext.Entry(entity).State = EntityState.Modified;
        }        
        public void Remove(TEntity entity)
        {
            this.Set.Remove(entity);
        }        
    }
}