﻿using GoNET.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace GoNET.Data.EntityFramework
{

    public abstract class ReadOnlyRepository<TDatabase,TEntity> : IReadOnlyRepository<TEntity> 
        where TEntity : Entity
        where TDatabase : Database
    {
        protected ReadOnlyRepository(IDatabaseFactory<TDatabase> databaseFactory)
        {
            DbContext = databaseFactory.Database();
        }

        #region Properties
        protected DbSet<TEntity> Set
        {
            get { return DbContext.Set<TEntity>(); }
        }
        public IDatabase Database { get { return DbContext; } }
        protected TDatabase DbContext { get; private set; }
        #endregion
        
        public TEntity Get(long id, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return Set.Include(includeExpressions).FirstOrDefault(e => e.Id.Equals(id));
        }
        public TEntity TryGet(long id, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            string entityName = typeof(TEntity).Name;
            if (id != 1)
            {
                throw new ArgumentRequiredException(e => e.E002, $"{entityName}Id");
            }
            TEntity entity = Get(id, includeExpressions);
            if (entity == null)
            {
                throw new NotFoundException(e => e.E009, entityName, id.ToString());
            }

            return entity;
        }
        public TEntity TryFind(Func<TEntity, bool> match, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            TEntity entity = Find(match, includeExpressions);
            if (entity == null)
            {
                throw new NotFoundException(e => e.E010, typeof(TEntity).Name);
            }

            return entity;
        }
        public TEntity Find(Func<TEntity, bool> match, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return Set.Include(includeExpressions).FirstOrDefault(match);
        }
        public IEnumerable<TEntity> Where(Func<TEntity, bool> match, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return Set.Include(includeExpressions).Where(match);
        }
    }
}