﻿using System.Data.Entity;
using System.Linq;
using System;
using System.Threading;

namespace GoNET.Data.EntityFramework
{
    using Exceptions;
    using System.Data.Entity.Validation;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    public abstract class Database : DbContext, IDatabase
    {
        public Database(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }               
        public void Commit()
        {
            try
            {
                SetAuditableFields();
                SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                ThrowDbValidationException(ex);
            }
            catch (Exception ex)
            {
                ThrowException(ex);
            }
        }
        public async Task CommitAsync()
        {
            try
            {
                SetAuditableFields();
                await SaveChangesAsync();
            }
            catch (DbEntityValidationException ex)
            {
                ThrowDbValidationException(ex);
            }
            catch (Exception ex)
            {
                ThrowException(ex);
            }
        }
        public void SetAuditableFields()
        {
            var modifiedEntries = ChangeTracker.Entries()
                            .Where(x => x.Entity is IAuditableEntity
                                && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                IAuditableEntity entity = entry.Entity as IAuditableEntity;
                if (entity != null)
                {
                    string identityName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime now = DateTime.UtcNow;

                    if (entry.State == EntityState.Added)
                    {
                        entity.CreatedBy = identityName;
                        entity.CreatedDate = now;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }

                    entity.UpdatedBy = identityName;
                    entity.UpdatedDate = now;
                }
            }
        }
        private void ThrowDbValidationException(DbEntityValidationException ex)
        {
            string[] validationErrors = ex.EntityValidationErrors?
                     .SelectMany(x => x.ValidationErrors)
                     .Select(x => x.ErrorMessage).ToArray();
            throw new DbValidationException(ex, validationErrors);
        }
        private void ThrowException(Exception exception)
        {
            var baseException = exception.GetBaseException();
            if (baseException is SqlException)
            {
                var sqlException = baseException as SqlException;
                if (sqlException.Number == 2601)//uniqueness
                {
                    string tableName = sqlException.Message.Match("dbo.", "'");
                    string uniqueValue = sqlException.Message.Match("unique_", "_");
                    throw new SqlDbException(e => e.E007, tableName, uniqueValue);//There’s already a {entity} with that {value}
                }
                else if (sqlException.Number == 547)//FK violation
                {
                    string tableName = sqlException.Message.Match("table \"dbo.", "\"");
                    throw new SqlDbException(e => e.E008, tableName);//There’s not a {entity} with that id.
                }
            }
            throw baseException;
        }
    }
}