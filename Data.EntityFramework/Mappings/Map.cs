﻿using System;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration;

namespace GoNET.Data.Mappings
{
    public abstract class Map<TEntityType> : EntityTypeConfiguration<TEntityType> where TEntityType : class
    {
        protected Map()
        {
            MapKey();
            MapProperties();
            MapRelationships();
            MapTable();
        }
        protected abstract void MapKey();
        protected abstract void MapProperties();
        protected abstract void MapRelationships();
        protected abstract void MapTable();

        public string IndexUniqueMultiple(Expression<Func<TEntityType, string>> firstProperty, Expression<Func<TEntityType, string>> secondProperty, bool maxLengthMinimum = false)
        {
            string firstPropertyName = (firstProperty.Body as MemberExpression).Member.Name;
            string secondPropertyName = (secondProperty.Body as MemberExpression).Member.Name;
            string uniqueIndexName = $"unique_{firstPropertyName}_{secondPropertyName}_index";
            var firstPropertyConfig = Property(firstProperty);
            IndexUniqueMultiple(Property(firstProperty), Property(secondProperty), uniqueIndexName, maxLengthMinimum);
            return uniqueIndexName;
        }
        public string IndexUniqueMultiple<TStruct>(Expression<Func<TEntityType, string>> firstProperty, Expression<Func<TEntityType, TStruct>> secondProperty, bool maxLengthMinimum = false) where TStruct : struct
        {
            string firstPropertyName = (firstProperty.Body as MemberExpression).Member.Name;
            string secondPropertyName = (secondProperty.Body as MemberExpression).Member.Name;
            string uniqueIndexName = $"unique_{firstPropertyName}_{secondPropertyName}_index";
            IndexUniqueMultiple(Property(firstProperty), Property(secondProperty), uniqueIndexName, maxLengthMinimum);
            return uniqueIndexName;
        }
        private void IndexUniqueMultiple(PrimitivePropertyConfiguration firstProperty, PrimitivePropertyConfiguration secondProperty, string uniqueIndexName, bool maxLengthMinimum = false)
        {
            if (maxLengthMinimum)
            {
                if (firstProperty is StringPropertyConfiguration)
                {
                    (firstProperty as StringPropertyConfiguration).HasMaxLength(400);
                }
                if (secondProperty is StringPropertyConfiguration)
                {
                    (secondProperty as StringPropertyConfiguration).HasMaxLength(400);
                }
            }
            IndexUnique(firstProperty, uniqueIndexName, 1);
            IndexUnique(secondProperty, uniqueIndexName, 2);
        }
        public string IndexUnique(Expression<Func<TEntityType, string>> property, bool maxLengthMinimum = false)
        {
            var propertyConfiguration = Property(property);
            if (maxLengthMinimum)
            {
                propertyConfiguration.HasMaxLength(400);
            }
            string propertyName = (property.Body as MemberExpression).Member.Name;
            string uniqueIndexName = $"unique_{propertyName}_index";
            IndexUnique(propertyConfiguration, uniqueIndexName, 1);
            return uniqueIndexName;
        }
        private void IndexUnique(PrimitivePropertyConfiguration propertyConfiguration, string indexName, int order)
        {
            propertyConfiguration.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute(indexName) { IsUnique = true, Order = order }));
        }
    }
}
