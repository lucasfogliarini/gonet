﻿namespace GoNET.Data.Mappings
{
    public abstract class AuditableEntityMap<TEntityType> : Map<TEntityType>  where TEntityType : AuditableEntity
    {
        protected AuditableEntityMap()
        {
            MapAuditableProperties();
        }

        private void MapAuditableProperties()
        {
            Property(e => e.CreatedBy).HasColumnName("CreatedBy").IsRequired();
            Property(e => e.CreatedDate).HasColumnName("CreatedDate").IsRequired();
            Property(e => e.UpdatedBy).HasColumnName("UpdatedBy");
            Property(e => e.UpdatedDate).HasColumnName("UpdatedDate");
        }
    }
}
