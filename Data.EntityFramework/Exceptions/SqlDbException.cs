﻿using System;
using System.Linq.Expressions;

namespace GoNET.Exceptions
{
    public class SqlDbException : DbException
    {
        public SqlDbException(Expression<Func<MessageCode.Messages, string>> code, params string[] args) : base(code, args)
        {
        }
    }
}
