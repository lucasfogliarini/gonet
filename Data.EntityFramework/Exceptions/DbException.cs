﻿using System;
using System.Linq.Expressions;

namespace GoNET.Exceptions
{
    public abstract class DbException : BaseException
    {
        public DbException(Expression<Func<MessageCode.Messages, string>> code, params string[] args) : base(code, args)
        {
        }
    }
}
