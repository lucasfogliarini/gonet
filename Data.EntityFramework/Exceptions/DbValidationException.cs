﻿using System.Data;

namespace GoNET.Exceptions
{
    public class DbValidationException : DbException
    {
        public DbValidationException(DataException dataException, string[] validationErrors) : base(c => c.E006, string.Join(", ", validationErrors))
        {
            DataException = dataException;
            ValidationErrors = validationErrors;
        }
        public DataException DataException { get; private set; }
        public string[] ValidationErrors { get; private set; }
    }
}
