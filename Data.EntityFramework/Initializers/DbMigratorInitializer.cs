﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Configuration;
using GoNET.Data.Seeds;

namespace GoNET.Data.EntityFramework
{
    public abstract class DbMigratorInitializer<TMigrationsConfiguration, TDbContext> : IDatabaseInitializer<TDbContext> 
        where TDbContext: DbContext
        where TMigrationsConfiguration : DbMigrationsConfiguration<TDbContext>, new()
    {
        const string environmentKey = "environment";
        public void InitializeDatabase(TDbContext context)
        {
            DbContext = context;
            DbMigrationsConfiguration = new TMigrationsConfiguration();
            
            OnInitialization();
            Environment = ConfigurationManager.AppSettings.GetAppEnvironment();
            DbMigrator migrator = new DbMigrator(DbMigrationsConfiguration);
            DbContext.Database.CommandTimeout = migrator.Configuration.CommandTimeout = DbContext.Database.Connection.ConnectionTimeout;
            migrator.Update();
            switch (Environment)
            {
                case AppEnvironment.Development:
                    SeedDevelopment();
                    break;
                case AppEnvironment.Test:
                    SeedTest();
                    break;
                case AppEnvironment.Homolog:
                    SeedHomolog();
                    break;
                case AppEnvironment.Production:
                    SeedProduction();
                    break;
            }
            DbContext.SaveChanges();
        }

        protected virtual void OnInitialization()
        {
        }
        public TMigrationsConfiguration DbMigrationsConfiguration { get; private set; }
        public TDbContext DbContext { get; private set; }
        protected AppEnvironment Environment { get; private set; }
        protected virtual void SeedDevelopment() { }
        protected virtual void SeedTest() { }
        protected virtual void SeedProduction()
        {
            SeedHistory();
        }
        protected virtual void SeedHomolog()
        {
            SeedHistory();
        }
        private void SeedHistory()
        {
            string environmentNameSpace = Environment == AppEnvironment.Production ? "Production" : "Homolog";
            var configurationType = DbMigrationsConfiguration.GetType();
            var seedTypes = configurationType.Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Seed)) && t.Namespace == $"{configurationType.Namespace}.Seeds.{environmentNameSpace}");
            foreach (var seedType in seedTypes)
            {
                Seed seed = Seed.CreateInstance(seedType, DbContext);
                var setSeedHistory = DbContext.Set<SeedHistory>();
                var seedHistory = new SeedHistory(seed);
                if (!setSeedHistory.Any(s => s.SeedId == seedHistory.SeedId))
                {
                    setSeedHistory.Add(seedHistory);
                    seed.Up();
                }
            }
        }
    }
}
