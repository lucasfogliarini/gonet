﻿using System.Text;
using System.Linq.Expressions;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;

namespace System.Data.Entity
{
    public static class EntityFrameworkExtensions
    {
        public static IQueryable<TEntity> Include<TEntity>(this DbSet<TEntity> dbSet, params Expression<Func<TEntity, object>>[] includeExpressions) where TEntity : class
        { 
            IQueryable<TEntity> query = dbSet;
            if (includeExpressions != null && includeExpressions.Any())
                query = includeExpressions.Aggregate(query, (current, expression) => current.Include(expression));
            return query;
        }
        public static void Clear<TEntity>(this DbContext dbContext) where TEntity : class
        {
            var tableName = typeof(TEntity).Name;
            dbContext.Database.ExecuteSqlCommand($"DELETE FROM [{tableName}]");
            dbContext.Database.ExecuteSqlCommand($"DBCC CHECKIDENT ('{tableName}',RESEED, 0)");
        }
        public static void Clear(this DbContext dbContext)
        {
            //nocheckConstraint
            string nocheckConstraint = "EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT all'";

            //clearTablesCommand
            string whereand = "AND o.id != OBJECT_ID(\"__MigrationHistory\")";
            string clearTablesCommand = $"EXEC sp_MSForEachTable @command1='DELETE FROM ?', @whereand='{whereand}'";

            //checkConstraint
            string checkConstraint = "EXEC sp_MSForEachTable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all'";

            //reselongentityCommand
            StringBuilder whereandSb = new StringBuilder();
            whereandSb.Append(" AND ");
            whereandSb.Append("(SELECT top 1 last_value");
            whereandSb.Append("	FROM sys.identity_columns ");
            whereandSb.Append("	WHERE object_id=o.id and last_value IS NOT NULL) IS NOT NULL"); //ref: http://goo.gl/dpxURS
            string reselongentityCommand = $"EXEC sp_MSForEachTable @command1='DBCC CHECKIDENT ([?], reseed, 0)', @whereand='{whereandSb}'";
            dbContext.Database.ExecuteSqlCommand($"{nocheckConstraint}; {clearTablesCommand}; {checkConstraint}; {reselongentityCommand};");
        }
        //public static void AddIfNotExists<T>(this DbSet<T> dbSet, params T[] entities) where T : Entity<long>
        //{
        //    foreach (var entity in entities)
        //    {
        //        bool exists = dbSet.Any(i=>i.Id == entity.Id);
        //        if (!exists)
        //        {
        //            dbSet.Add(entity);
        //        }
        //    }
        //}
        public static IReadOnlyCollection<DbEntityEntry> GetAdditions(this DbContext dbContext)
        {
            return dbContext.ChangeTracker.Entries().Where(p => p.State == EntityState.Added).ToList().AsReadOnly();
        }
        public static IReadOnlyCollection<DbEntityEntry> GetModifications(this DbContext dbContext)
        {
            return dbContext.ChangeTracker.Entries().Where(p => p.State == EntityState.Deleted || p.State == EntityState.Modified).ToList().AsReadOnly();
        }
    }
}
