﻿namespace System
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Verify if a date are between others two dates.
        /// </summary>
        /// <param name="date">the date to be verify.</param>
        /// <param name="start">the start date.</param>
        /// <param name="end">the end date.</param>
        /// <returns></returns>
        public static bool Between(this DateTime date, DateTime start, DateTime end)
        {
            return date >= start && date <= end;
        }

        public static DateTime ClearSeconds(this DateTime self)
        {
            return self.AddSeconds(-self.Second);
        }

        public static DateTime ClearMilliSeconds(this DateTime self)
        {
            return self.AddMilliseconds(-self.Millisecond);
        }
    }
}
