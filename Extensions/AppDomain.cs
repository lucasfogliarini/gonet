﻿using System.IO;

namespace System
{
    public static class AppDomainExtensions
    {
        public static string GetPath(this AppDomain appDomain, string path)
        {
            return Path.Combine(appDomain.RelativeSearchPath, path);
        }
    }
}
