﻿using System.Collections;
namespace System
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Verify if null or IEnumerable Empty
        /// </summary>
        public static bool HasValue<T>(this T input) where T : class
        {
            if (input == null) return false;
            return input is IEnumerable ? (input as IEnumerable).GetEnumerator().MoveNext() : false;
        }
    }
}
