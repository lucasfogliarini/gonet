﻿using System.Linq;

namespace System.Collections.Generic
{
    public static class IEnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            if(collection != null)
                foreach (T item in collection)
                    action(item);
        }
        public static IEnumerable<TLeft> Outer<TLeft, TRight>(this IEnumerable<TLeft> collection, IEnumerable<TRight> rightItems, Func<TLeft, TRight, bool> predicate) where TRight : TLeft
        {
            foreach (TLeft leftItem in collection)
            {
                bool contains = rightItems.Any(right => predicate(leftItem, right));
                if (!contains)
                {
                    yield return leftItem;
                }
            }
        }
    }
}
