﻿namespace GoNET.Services
{
    using System;
    using Data;
    using Exceptions;

    public abstract class Service
    {
        private readonly IDatabase _database;
        public Service(IDatabase database)
        {
            _database = database;
        }
        protected void Commit()
        {
            if (CanCommit)
            {
                _database.Commit();
            }
        }
        protected bool CanCommit { get; private set; } = true;
        protected void DependentServices(params Service[] services)
        {
            foreach (Service service in services)
            {
                service.CanCommit = false;
            }
        }
        protected virtual void TryCatch(Action _try)
        {
            _tryCatch(_try);
        }
        protected virtual TOut TryCatch<TOut>(Func<TOut> _try)
        {
            TOut _out = default(TOut);
            _tryCatch(() =>
            {
                _out = _try();
            });
            return _out;
        }
        private void _tryCatch(Action _try)
        {
            try
            {
                _try();
            }
            catch (BaseException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exceptions.SystemException(ex);
            }
        }

        /// <summary>
        /// Throw <see cref="NullException"/> if null.
        /// </summary>
        protected void ThrowIfNull<TEntity>(TEntity entity) where TEntity : Entity
        {
            if (entity == null)
            {
                throw new NullException(typeof(TEntity).Name);
            }
        }
        /// <summary>
        /// Throw <see cref="InactiveException"/> if Active is false. (E006)
        /// </summary>
        protected void ThrowIfInactive<TEntity>(TEntity entity) where TEntity : ISwitch
        {
            if (!entity.Active)
            {
                throw new InactiveException(e => e.E004, typeof(TEntity).Name);
            }
        }
    }
}