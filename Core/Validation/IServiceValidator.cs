﻿namespace GoNET.Data.Validation
{
    public interface IServiceValidator
    {
        IModelValidator ModelValidator { get; }
    }
}
