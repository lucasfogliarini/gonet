﻿namespace GoNET.Data.Validation
{
    public interface IValidationError
    {
        string ErrorMessage { get; }
        string PropertyName { get; }
        string ClassName { get; }
    }
}
