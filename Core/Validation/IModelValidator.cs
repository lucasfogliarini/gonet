﻿using System.Collections.Generic;

namespace GoNET.Data.Validation
{
    public interface IModelValidator
    {
        IEnumerable<IValidationError> Errors { get; }
        bool HasErrors { get; }
        void Clear();
        void ThrowErrors();
        void Validate<TEntity>(TEntity entity);
    }
}