﻿using GoNET.Exceptions;
using System;
using System.Collections.Generic;

namespace GoNET.Data.Validation
{
    [Serializable]
    public class ValidationException : InvalidException
    {
        public ValidationException(string message , IEnumerable<IValidationError> errors)
            : base(c => c.E005, message)
        {
            Errors = errors;
        }
        public IEnumerable<IValidationError> Errors { get; private set; }
    }

}
