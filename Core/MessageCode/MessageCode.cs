﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace GoNET
{
    public partial class MessageCode
    {
        public MessageCode(Expression<Func<Messages, string>> code, params object[] args) : this(code.Body as MemberExpression, args)
        {
        }
        protected MessageCode(MemberExpression memberExpression, params object[] args)
        {            
            Message = (memberExpression.Member as FieldInfo).GetValue(new Messages()) as string;
            Message = string.Format(Message, args);
            Code = memberExpression.Member.Name;
        }
        public string Message { get; set; }
        public string Code { get; set; }
    }
}
