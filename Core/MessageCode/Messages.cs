﻿namespace GoNET
{
    public partial class MessageCode
    {
        public class Messages
        {
            public readonly string E001 = "Ops! An error occurred in the system. Exception message: {0}. Inner exception message: {1}";
            public readonly string E002 = "{0} can't be null.";
            public readonly string E003 = "{0} is required.";
            public readonly string E004 = "{0} must be implemented.";
            public readonly string E005 = "{0} is inactive.";
            public readonly string E006 = "The entity(s) model(s) have invalid fields. See errors: {0}";
            public readonly string E007 = "Database validation exception. Messages: {0}";
            public readonly string E008 = "There’s already a {0} with that {1}.";
            public readonly string E009 = "There’s not a {0} with that id.";
            public readonly string E010 = "The {0} with id '{1}' was not found.";
            public readonly string E011 = "The {0} was not found.";
            public readonly string E012 = "Unauthorized.";
            public readonly string E013 = "Unauthenticated.";
            public readonly string E014 = "Refresh token sent has a different clientId.";
        }
    }
}
