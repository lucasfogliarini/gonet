﻿using System;

namespace GoNET.Data
{
    public abstract class DatabaseFactory<TDatabase> : IDatabaseFactory<TDatabase>, IDisposable
        where TDatabase : IDatabase, new()
    {
        TDatabase _context;
        public TDatabase Database()
        {
            if(_context == null)
            {
                _context = new TDatabase();
            }
            return _context;
        }
                
        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}
