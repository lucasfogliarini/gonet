﻿using System;
using System.Threading.Tasks;

namespace GoNET.Data
{
    public interface IDatabase : IDisposable
    {
        void SetAuditableFields();
        void Commit();
        Task CommitAsync();
    }
}
