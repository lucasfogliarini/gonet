﻿namespace GoNET.Data
{
    public interface IDatabaseFactory<TDatabase> where TDatabase : IDatabase
    {
        TDatabase Database();
    }
}