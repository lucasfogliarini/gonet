﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace GoNET.Data
{
    public interface IReadOnlyRepository<TEntity>
    {
        IDatabase Database { get; }
        TEntity Get(long id, params Expression<Func<TEntity, object>>[] includeExpressions);
        /// <summary>
        /// Try to get the Entity, if null throw NotFoundException
        /// </summary>
        TEntity TryGet(long id, params Expression<Func<TEntity, object>>[] includeExpressions);
        TEntity Find(Func<TEntity, bool> match, params Expression<Func<TEntity, object>>[] includeExpressions);
        TEntity TryFind(Func<TEntity, bool> match, params Expression<Func<TEntity, object>>[] includeExpressions);
        IEnumerable<TEntity> Where(Func<TEntity, bool> match, params Expression<Func<TEntity, object>>[] includeExpressions);
    }
}