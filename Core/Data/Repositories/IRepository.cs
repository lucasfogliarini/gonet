﻿namespace GoNET.Data
{
    public interface IRepository<TEntity> : IReadOnlyRepository<TEntity>
    {
        TEntity Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(TEntity entity);
    }
}