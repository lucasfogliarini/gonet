﻿namespace GoNET.Exceptions
{
    public class NotImplementedException : BaseException
    {
        public NotImplementedException(string description) : base(c => c.E003, description)
        {           
        }
    }
}
