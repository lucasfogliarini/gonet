﻿using System;
using System.Linq.Expressions;

namespace GoNET.Exceptions
{
    public abstract class BaseException : Exception
    {
        protected BaseException(Expression<Func<MessageCode.Messages, string>> code, params object[] args)
        {
            MessageCode = new MessageCode(code, args);
        }
        public MessageCode MessageCode { get; private set; }
        public override string Message
        {
            get
            {
                return MessageCode.Message;
            }
        }
        public string Code { get { return MessageCode.Code; } }
    }
}
