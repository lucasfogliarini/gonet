﻿using System;
using System.Linq.Expressions;

namespace GoNET.Exceptions
{
    public class InactiveException : BaseException
    {
        public InactiveException(Expression<Func<MessageCode.Messages, string>> code, params string[] args) :  base(code)
        {
        }
    }
}
