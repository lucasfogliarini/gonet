﻿using System;

namespace GoNET.Exceptions
{
    public class SystemException : BaseException
    {
        public SystemException(Exception ex)
            : base(c => c.E001, ex.Message, ex.InnerException?.Message)
        {
        }
    }
}
