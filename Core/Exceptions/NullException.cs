﻿namespace GoNET.Exceptions
{
    public class NullException : BaseException
    {
        public NullException(params string[] args) :  base(e=>e.E002, args)
        {
        }
    }
}
