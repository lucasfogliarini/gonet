﻿using System;
using System.Linq.Expressions;

namespace GoNET.Exceptions
{
    public class NotFoundException : BaseException
    {
        public NotFoundException(Expression<Func<MessageCode.Messages, string>> code, params string[] args) :  base(code, args)
        {
        }
    }
}
