﻿using System;
using System.Linq.Expressions;

namespace GoNET.Exceptions
{
    public class InvalidException : BaseException
    {
        public InvalidException(Expression<Func<MessageCode.Messages, string>> code, params string[] args) :  base(code, args)
        {
        }
    }
}
