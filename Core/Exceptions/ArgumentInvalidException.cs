﻿using System;
using System.Linq.Expressions;

namespace GoNET.Exceptions
{
    /// <summary>
    /// Exception thrown when a one of the arguments provided to a method is not valid.
    /// </summary>
    public class ArgumentInvalidException : BaseException
    {
        public ArgumentInvalidException(Expression<Func<MessageCode.Messages, string>> code, params object[] args) : base(code, args)
        {
        }
    }
}
