﻿namespace GoNET.Data
{
    public interface ISwitch
    {
        bool Active { get; }
    }
}