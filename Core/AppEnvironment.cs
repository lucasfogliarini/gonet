﻿using System.Collections.Specialized;
using System.ComponentModel;

namespace GoNET
{
    public enum AppEnvironment
    {
        [Description("development")]
        Development,
        [Description("test")]
        Test,
        [Description("homolog")]
        Homolog,
        [Description("production")]
        Production
    }

    public static class AppEnvironmentExtensions
    {
        const string environmentKey = "environment";
        public static AppEnvironment GetAppEnvironment(this NameValueCollection appSettings)
        {
            var environment = appSettings[environmentKey];
            switch (environment)
            {
                case "test":
                    return AppEnvironment.Test;
                case "production":
                    return AppEnvironment.Production;
                case "homolog":
                    return AppEnvironment.Homolog;
                default:
                    return AppEnvironment.Development;
            }
        }
    }    
}
