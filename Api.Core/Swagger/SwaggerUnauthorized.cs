﻿namespace GoNET.Api.Swagger
{
    public class SwaggerUnauthorized : SwaggerStatusCode
    {
        public SwaggerUnauthorized() : base(nameof(MessageStatusCode.E002))
        {
        }
    }
}