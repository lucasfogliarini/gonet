﻿namespace GoNET.Api.Swagger
{
    public class SwaggerInternalServerError : SwaggerStatusCode
    {
        public SwaggerInternalServerError() : base(nameof(MessageStatusCode.E001))
        {
        }
    }
}