﻿using Swashbuckle.Swagger.Annotations;
using System;
using System.Net;

namespace GoNET.Api.Swagger
{
    public class SwaggerOk : SwaggerResponseAttribute
    {
        public SwaggerOk(Type type = null) : base(HttpStatusCode.OK)
        {
            Type = type;
        }
        public SwaggerOk(params string[] codes) : base(HttpStatusCode.Created, SwaggerStatusCode.GetDescription(codes))
        {
            Description += "<br /> <b>note:</b> The StatusCode is actually 'Ok(200)'";
        }
    }
}