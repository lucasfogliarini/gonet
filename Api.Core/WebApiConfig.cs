﻿using System.Web.Http;

namespace GoNET.Api
{
    /// <summary>
    /// Web api global configuration.
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Register global configuration for api.
        /// </summary>
        /// <param name="config">api configuration.</param>
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
