﻿using System.Net;

namespace GoNET.Api
{
    public static class MessageStatusCode
    {
        public static HttpStatusCode StatusCode(this MessageCode messageCode)
        {
            var statusCodeField = typeof(MessageStatusCode).GetField(messageCode.Code);
            if (statusCodeField == null)
            {
                throw new Exceptions.NotImplementedException("MessageStatusCode");
            }
            return (HttpStatusCode)statusCodeField.GetValue(null);
        }
        public const HttpStatusCode E001 = HttpStatusCode.InternalServerError;
        public const HttpStatusCode E002 = HttpStatusCode.BadRequest;
        public const HttpStatusCode E003 = HttpStatusCode.BadRequest;
        public const HttpStatusCode E004 = HttpStatusCode.BadRequest;
        public const HttpStatusCode E005 = HttpStatusCode.BadRequest;
        public const HttpStatusCode E006 = HttpStatusCode.BadRequest;
        public const HttpStatusCode E007 = HttpStatusCode.BadRequest;
        public const HttpStatusCode E008 = HttpStatusCode.BadRequest;
        public const HttpStatusCode E009 = HttpStatusCode.BadRequest;
        public const HttpStatusCode E010 = HttpStatusCode.NotFound;
        public const HttpStatusCode E011 = HttpStatusCode.NotFound;
        public const HttpStatusCode E012 = HttpStatusCode.Forbidden;
        public const HttpStatusCode E013 = HttpStatusCode.Unauthorized;
        public const HttpStatusCode E014 = HttpStatusCode.BadRequest;

    }
}
