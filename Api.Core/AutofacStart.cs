﻿using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;

namespace GoNET.Api
{
    public abstract class AutofacStart : StartBase
    {
        protected override void Up()
        {
            RegisterDependencies(new ContainerBuilder());
            base.Up();
        }
        protected abstract Assembly[] AutofacModules { get; }
        protected virtual Assembly[] ControllerAssemblies { get; }
        protected virtual void RegisterDependencies(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterAssemblyModules(AutofacModules);
            RegisterApiControllers(containerBuilder);
        }
        private void RegisterApiControllers(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterApiControllers(GetType().Assembly, typeof(Controllers.BaseController).Assembly);
            if(ControllerAssemblies != null) containerBuilder.RegisterApiControllers(ControllerAssemblies);
        }
    }
}