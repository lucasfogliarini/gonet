﻿using System;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Swashbuckle.Application;

namespace GoNET.Api
{
    public abstract class StartBase
    {
        protected HttpConfiguration HttpConfiguration { get; set; } = new HttpConfiguration();
        protected abstract string XmlCommentsFileName { get; }
        protected abstract string Title { get; }
        protected abstract string ApiVersion { get; }
        protected virtual void Up()
        {
            ConfigureJsonSerialization();
            WebApiConfig.Register(HttpConfiguration);
            HttpConfiguration.EnableSwagger(c =>
             {
                 c.SingleApiVersion(ApiVersion, Title);
                 c.IncludeXmlComments(AppDomain.CurrentDomain.GetPath(XmlCommentsFileName));
             }).EnableSwaggerUi();
        }

        private void ConfigureJsonSerialization()
        {
            HttpConfiguration.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }
    }
}