﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using System.Security.Claims;

namespace GoNET.Api.Controllers
{
    using Exceptions;
    using System.Threading;
    using System.Threading.Tasks;
    public abstract class BaseController : ApiController
    {
        public BaseController()
        {
            Thread.CurrentPrincipal = User;
        }
        protected IHttpActionResult TryCatch(Func<IHttpActionResult> _try)
        {
            try
            {
                return _try();
            }
            catch (BaseException ex)
            {
                return Content(ex.MessageCode.StatusCode(), ex.MessageCode);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        protected async Task<IHttpActionResult> TryCatchAsync(Func<Task<IHttpActionResult>> _try)
        {
            try
            {
                return await _try();
            }
            catch (BaseException ex)
            {
                return Content(ex.MessageCode.StatusCode(), ex.MessageCode);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        } 
        protected override ExceptionResult InternalServerError(Exception exception)
        {
            return base.InternalServerError(new SystemException(exception));
        }
        protected CreatedNegotiatedContentResult<T> Created<T>(T content, string location = "")
        {
            return Created(location, content);
        }
        protected ClaimsIdentity ClaimsIdentity
        {
            get
            {
                var identity = User?.Identity;
                if (identity is ClaimsIdentity)
                {
                    return identity as ClaimsIdentity;
                }
                return null;
            }
        }       
    }
}