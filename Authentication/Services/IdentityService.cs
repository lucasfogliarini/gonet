﻿using GoNET.Data;
using System.Security.Claims;
using System;
using GoNET.Exceptions;
using System.Threading;
using System.Diagnostics;
using GoNET.Authentication;
using System.Reflection;

namespace GoNET.Services.Authentication
{
    public abstract class IdentityService : Service
    {
        public IdentityService(IDatabase database) : base(database)
        {
        }
        public UserIdentity User
        {
            get
            {
                return Thread.CurrentPrincipal.Identity is UserIdentity ? (Thread.CurrentPrincipal.Identity as UserIdentity) : null;
            }
        }
        MethodBase _calledMethod;

        protected override void TryCatch(Action _try)
        {
            _calledMethod = new StackTrace().GetFrame(1).GetMethod();
            base.TryCatch(() =>
            {
                VerifyAuthorization();
                _try();
            });
        }
        protected override TOut TryCatch<TOut>(Func<TOut> _try)
        {
            _calledMethod = new StackTrace().GetFrame(1).GetMethod();
            return base.TryCatch(()=>
            {
                VerifyAuthorization();
                return _try();
            });
        }
        private void VerifyAuthorization()
        {
            bool authenticated = User != null && User.IsAuthenticated;
            if (!authenticated)
            {
                throw new AuthenticationException();
            }
            if (!Authorize())
            {
                throw new AuthorizationException();
            }
        }
        protected virtual bool Authorize()
        {
            string serviceName = _calledMethod.DeclaringType.Name;
            string permission = $"{serviceName}.{_calledMethod.Name}";
            return User.HasPermission(permission);
        }
    }
}
