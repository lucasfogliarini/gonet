﻿using GoNET.Authentication;
using System;

namespace GoNET.Services.Authentication
{
    public interface IAuthenticationService
    {
        UserIdentity Authorize(string userName);
        IRefreshToken CreateRefreshToken(string clientId, string name, DateTime issuedDate, DateTime expiresDate, string serializeTicket);
        string RemoveRefreshToken(string token);
        IAppClient GetAppClient(string clientId);
    }
}
