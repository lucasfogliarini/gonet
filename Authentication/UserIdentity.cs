﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace GoNET.Authentication
{
    public class UserIdentity : ClaimsIdentity
    {
        public UserIdentity(string id, string userName, IEnumerable<string> permissions, string authorizationType = authorizationTypeDefault) : base(authorizationType)
        {
            permissions.ForEach(p => AddPermission(p));
            this.IncludeClaim(ClaimTypes.Name, userName);
            this.IncludeClaim(IdUserAuthenticatedClaimType, id);
        }
        public const string IdUserAuthenticatedClaimType = "userId";
        public const string PermissionClaimType = "permission";
        const string authorizationTypeDefault = "none";
        public void AddPermission(string permission)
        {
            this.IncludeClaim(PermissionClaimType, permission);
        }
        public IEnumerable<Claim> Permissions { get { return Claims.Where(e => e.Type == PermissionClaimType); } }
        public bool HasPermission(string permission)
        {
            return HasClaim(PermissionClaimType, permission);
        }
    }

    public static class ClaimsIdentityConverterExtension
    {
        public static UserIdentity ToUserIdentity(this ClaimsIdentity claimsIdentity)
        {
            string id = claimsIdentity.FindFirst(UserIdentity.IdUserAuthenticatedClaimType).Value;
            string userName = claimsIdentity.Name;
            var permissions = claimsIdentity.Claims.Where(c => c.Type == UserIdentity.PermissionClaimType).Select(c=>c.Value);
            return new UserIdentity(id,userName, permissions);
        }
    }
}
