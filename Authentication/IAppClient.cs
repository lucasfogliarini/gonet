﻿namespace GoNET.Authentication
{
    public interface IAppClient
    {
        string AllowedOrigin { get; set; }
        string RefreshTokenLifeTime { get; set; }
    }
}
