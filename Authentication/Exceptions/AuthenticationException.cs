﻿namespace GoNET.Exceptions
{
    /// <summary>
    /// Exception thrown when the user is unauthenticated.
    /// </summary>
    public class AuthenticationException : BaseException
    {
        public AuthenticationException() : base(m=>m.E012)
        {
        }
    }
}
