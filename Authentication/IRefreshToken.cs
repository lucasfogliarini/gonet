﻿namespace GoNET.Authentication
{
    public interface IRefreshToken
    {
        string RefreshTokenId { get; set; }
    }
}
