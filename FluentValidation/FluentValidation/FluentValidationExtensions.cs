﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;

namespace FluentValidation
{
    public static class ValidationFailureExtension
    {
        public static string EntityName(this ValidationFailure validationFailure)
        {
            return validationFailure.ResourceName;
        }
    }

    internal static class ValidatorExtensions
    {
        public static void AtLeast<T, TCollectionElement>(this IRuleBuilder<T, IEnumerable<TCollectionElement>> ruleBuilder, int length)
        {
            ruleBuilder.SetValidator(new AtLeastValidator<TCollectionElement>(length));
        }
        public static IRuleBuilderOptions<T, TProperty?> NullOrGreaterThan<T, TProperty>(this IRuleBuilder<T, TProperty?> ruleBuilder, TProperty valueToCompare) where TProperty : struct, IComparable<TProperty>, IComparable
        {
            return ruleBuilder.SetValidator(new NullOrGreaterThanValidator(valueToCompare));
        }
    }
}
