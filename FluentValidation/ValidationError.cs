﻿using FluentValidation.Results;
using GoNET.Data.Validation;

namespace GoNET.Validation.Fluent
{
    public class ValidationError : ValidationFailure, IValidationError
    {
        public ValidationError(ValidationFailure validationFailure, string className) : base(validationFailure.PropertyName, validationFailure.ErrorMessage, validationFailure.AttemptedValue)
        {
            ErrorCode = validationFailure.ErrorCode;
            ClassName = className;
        }
        public string ClassName { get; private set; }
    }
}
