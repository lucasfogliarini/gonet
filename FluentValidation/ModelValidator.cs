﻿using FluentValidation;
using FluentValidation.Results;
using GoNET.Data;
using GoNET.Data.Validation;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System;

namespace GoNET.Validation.Fluent
{
    public class ModelValidator : IModelValidator
    {
        readonly List<ValidationError> _errors = new List<ValidationError>(); 
        public IEnumerable<IValidationError> Errors
        {
            get
            {
                return _errors;
            }
        }
        public bool HasErrors { get { return _errors.Count > 0; } }
        public void ThrowErrors()
        {
            if (!HasErrors)
                return;

            StringBuilder validationMessage = new StringBuilder();
            foreach (ValidationFailure error in Errors)
            {
                if (validationMessage.Length > 0)
                    validationMessage.Append(" ");

                if (error.EntityName() != "")
                    validationMessage.Append(error.EntityName() + ": ");

                validationMessage.Append(error.ErrorMessage);
            }

            if (HasErrors)
            {
                Clear();
                throw new Data.Validation.ValidationException(validationMessage.ToString(), Errors);
            }
        }
        public void Clear()
        {
            _errors.Clear();
        }
        /// <summary>
        /// Verify if a entity is valid. Must have a implementation of <see cref="AbstractValidator{Entity}"/> in the <see cref="Entity"/> assembly.
        /// </summary>
        public void Validate<TEntity>(TEntity entity)
        {
            IValidator validator = GetValidator<TEntity>();
            ValidationResult validation = validator.Validate(entity);
            if (!validation.IsValid)
            {
                string entityName = entity.GetType().Name;
                var validationErrors = validation.Errors.Select(v => new ValidationError(v, entityName));
                _errors.AddRange(validationErrors);
            }
        }
        private IValidator GetValidator<TEntity>()
        {
            var entityType = typeof(TEntity);
            var validatorType = entityType.Assembly.GetTypes().FirstOrDefault(t => t.IsSubclassOf(typeof(AbstractValidator<TEntity>)));
            if (validatorType == null)
                throw new Exceptions.NotImplementedException($"AbstractValidator<{entityType.Name}> in the '{entityType.FullName}' assembly");
            return (IValidator)Activator.CreateInstance(validatorType);
        }
    }
}
